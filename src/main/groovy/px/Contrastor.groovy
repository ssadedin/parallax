package px

import java.util.List

import org.apache.commons.math3.random.JDKRandomGenerator
import org.apache.commons.math3.random.RandomGenerator
import graxxia.*

import gngs.*
import groovy.transform.CompileStatic
import groovy.transform.ToString
import groovy.util.logging.Log

@CompileStatic
@ToString(includeNames=true)
class Contrast {
    
    static RandomGenerator normalRandom = new JDKRandomGenerator()
    
    public Contrast(int position, LayeredCounts layers, List<Double> squaredErrors, List<List<RollingStats>> stats) {
        this.position = position;
        this.squaredErrors = squaredErrors;
        this.layers = layers
        this.stats = initStats(stats)
    }
    
    List<List<FastNormal>> initStats(List<List<RollingStats>> stats) {
        
//        if(position == 27169000){
//            println "debug"
//        }
//        
        List<List<FastNormal>> result = new ArrayList(stats.size())
        for(List<RollingStats> descs : stats) {
            List<FastNormal> dists = new ArrayList(descs.size())
            final int numDescs = descs.size()
            for(int i=0; i<numDescs; ++i) {
                if(i == 1) {
                    dists.add(null)
                }
                else {
                    RollingStats desc = descs.getAt(i)
                    double sd = desc.getStandardDeviation()
                    if(desc.mean > 0 && sd > 0) {
                        if(sd.isNaN()) {
                            println "oh no, naughty sd"
                        }
                        dists.add(new FastNormal(normalRandom, desc.mean, sd))
                    }
                    else
                        dists.add(null)
                }
            }
            result.add(dists)
        }
        return result
    }
    
    List<Double> getProfileLikelihoods(final double w1Factor, final double w3Factor) {
        int i = 0
        List<Double> result = new ArrayList()
        for(List<FastNormal> layerStats : stats){
            LayerCount layer = this.layers.counts[i++]
            result.add(getProfileLikelihood(layer, layerStats, w1Factor, w3Factor))
        }
        return result;
    } 
    
    final List<Double> getDeletionLikelihoods() {
        int i = 0
        final List<Double> result = new ArrayList()
        for(List<FastNormal> layerStats : stats){
            LayerCount layer = this.layers.counts[i++]
            result.add(getProfileLikelihood(layer, layerStats, 0.5d, 0.5d))
        }
        return result;
    }
    
    final double getProfileLikelihood(final LayerCount layer, final List<FastNormal> layerDists, final double n1Factor, final double n3Factor) {
        
        if(layerDists[0] == null || layerDists[2] == null)
            return -1d
            
        // A bit arbitrary: but we want the standard deviation used to be consistent so that the 
        // densities of the normal distributions are comparable (see below - using density not probability)
        final double sd = (layerDists[0].standardDeviation + layerDists[2].standardDeviation) / 2
        
        // Likelihood of component 1 normal: in that case it has the
        // same distribution as the two other components
//        final FastNormal n1 = new FastNormal(normalRandom, layer.readsPerBase[0], sd)
        final FastNormal n1 = layerDists[0]
//        FastNormal n2 = layerDists[1]
        final FastNormal n3 = layerDists[2]
//        final FastNormal n3 = new FastNormal(normalRandom, layer.readsPerBase[2], sd)
        
        final double n2rpb = layer.readsPerBase[1]
        
//        final FastNormal n1ExpectedDist = new FastNormal(normalRandom, n1.mean*n1Factor, n1.standardDeviation)
//        final FastNormal n3ExpectedDist = new FastNormal(normalRandom, n3.mean*n2Factor, n3.standardDeviation)
//        
        final FastNormal n1ExpectedDist = new FastNormal(normalRandom, n1.mean*n1Factor, n1.standardDeviation)
        final FastNormal n3ExpectedDist = new FastNormal(normalRandom, n3.mean*n3Factor, n3.standardDeviation)
        
//        final FastNormal n1ExpectedDist = new FastNormal(normalRandom,layer.readsPerBase[0]*n1Factor, n1.standardDeviation)
//        final FastNormal n3ExpectedDist = new FastNormal(normalRandom,layer.readsPerBase[2]*n3Factor, n3.standardDeviation)
        
        final double n1LR = n1ExpectedDist.logDensity(n2rpb) - n1.logDensity(n2rpb)
        final double n3LR = n3ExpectedDist.logDensity(n2rpb) - n3.logDensity(n2rpb)
  
        // TODO: to be theoretically sound, we should actually be using probabilities, not densities. 
        // However I have not had time to deal with numerical issues that are affecting the stability of these.
//        final NormalLikelihood n1ExpectedDist = new NormalLikelihood(layer.readsPerBase[0]*n1Factor, Math.max(n1.standardDeviation,n1.mean/10))
//        final NormalLikelihood n3ExpectedDist = new NormalLikelihood(layer.readsPerBase[2]*n2Factor, Math.max(n3.standardDeviation, n3.mean/10))
//  
//        final n1Dist = new NormalLikelihood(n1)
//        final n3Dist = new NormalLikelihood(n3)
//        
//        final double n1LR = n1ExpectedDist.ratio(n2rpb,n1Dist)
//        final double n3LR = n3ExpectedDist.ratio(n2rpb,n3Dist)
//        
//         final double score = Math.min(n1LR , n3LR)
//         final double score = (n1LR + n3LR)/2
        final double score = n1LR + n3LR
        
//        if(position == 20952223 && layer.widths[0] == 5000) {
//            println "debug: rpb: ${layer.readsPerBase.join(',')} " + layer.widths.join(',') + " n1=$n1, n2=$n2rpb, n3=$n3 (n3CnvDens=${n3ExpectedDist.logDensity(n2rpb)},n3dens=${n3.logDensity(n2rpb)}) with LR $n1LR,$n3LR total=" + (n1LR + n3LR) + " score=$score"
//        }
//        
        if(score.isNaN()) {
            println "oh no, its nan"
        }
        
        return score
    } 
    
    double getDeletionLikelihood(LayerCount layer, List<FastNormal> layerDists) {
        
        // Likelihood of component 1 normal: in that case it has the
        // same distribution as the two other components
        FastNormal n1 = layerDists[0]
//        FastNormal n2 = layerDists[1]
        FastNormal n3 = layerDists[2]
        
        if(n1 == null || n3 == null)
            return -1d
        
        FastNormal n1del = new FastNormal(normalRandom, n1.mean/2, n1.standardDeviation)
        FastNormal n3del = new FastNormal(normalRandom, n3.mean/2, n3.standardDeviation)
        
        double n2rpb = layer.readsPerBase[1]
        
        double n1DeletionLR = n1del.logDensity(n2rpb) - n1.logDensity(n2rpb)
        double n3DeletionLR = n3del.logDensity(n2rpb) - n3.logDensity(n2rpb)
        
//        if(position == 29100310 && layer.widths[0] == 5000) {
//            println "debug: " + layer.widths.join(',') + " with LR n1del $n1DeletionLR, $n3DeletionLR overall = " + (n1DeletionLR + n3DeletionLR)
//        }
//        
        double totalLR = n1DeletionLR + n3DeletionLR
        
        if(totalLR.isNaN()) {
            println "oh no, its nan"
        }
        
        return totalLR
    }
    
    int position
    
    List<Double> squaredErrors
    
    List<List<FastNormal>> stats
    
    LayeredCounts layers
}

@CompileStatic
@Log
class Contrastor extends RegulatingActor<LayeredCounts> {
    
    int pos = 0
    
    LayeredCounts lastCounts
    
    List<Integer> widths
    
    List<List<RollingStats>> stats 
    
    final int numComponents = 3
    
    final int numLayers
    
    public Contrastor(List<Integer> widths, RegulatingActor<Contrast> downstream) {
        super(downstream, 10000, 50000)
        this.widths = widths;
        this.stats = widths.collect { int w -> (1..numComponents).collect { 
            // why w/2 and not w?
            // The reason is that the rolling stats lag behind the window that they are sourcing
            // their values from. So a full w means that the stats window overlaps the previous 
            // layer component
            //              <------- w ------------>     
            //             |------------------------|
            // |------------------------|------------------------|
            //  <------- w ------------> <------- w ------------>
            //
            new RollingStats(w>>1i) } 
        }
        this.numLayers = this.widths.size()
    }
    
    ProgressCounter progress = new ProgressCounter(withRate:true, log:log, extra: {
        "Last position: $pos, counts=$lastCounts"
    })
    
    List<Double> lastRatios = null

    @Override
    public void process(LayeredCounts counts) {
        pos = counts.pos
        lastCounts = counts
        
        // Update the statistics for all the components of all the layers
        updateLayerStatistics(counts)
        
        // Note: specific to 3 component layer with CNV as middle
        // Other layer structures would need to change this
//        List error = counts.counts*.readsPerBase.collect {  double [] rpb ->
//            double delta1 = (0.5 - rpb[1] / rpb[0])
//            double delta2 = (0.5 - rpb[1] / rpb[2]) 
//            delta1*delta1 + delta2*delta2
//        }
        
        List error = null
        
        sendDownstream(new Contrast(counts.pos, counts, error, stats))
        
        progress.count()
    }

    private updateLayerStatistics(LayeredCounts counts) {
        for(int i=0; i<widths.size(); ++i) {
            for(int j=0; j<numComponents; ++j) { 
                stats[i][j].addValue(counts.counts[i].readsPerBase[j])
            }
        }
    }
    
    @Override
    public void onEnd() {
        progress.end()
    }
}
