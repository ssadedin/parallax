package px

import java.util.List

import gngs.*

import groovy.transform.CompileStatic
import groovy.util.logging.Log
import htsjdk.samtools.SAMRecord

@CompileStatic
interface ReadCountFilter {
    boolean isFilteredOut(SAMRecord r) 
}

@Log
class ReadCounterActor extends RegulatingActor<List<SAMRecord>> {
    private List<Integer> windowSizes
    
    final LayeredCoverageWindow window
    
    final int windowSize
    
    final int halfWindowSize 
    
    final String chr
    final int start 
    final int end
    
    /**
     * User settable filter to remove reads from consideration
     */
    ReadCountFilter filterFn
     
    int currentPosition 
    
    public ReadCounterActor(Region region, List<Integer> windowSizes, RegulatingActor<ReadWindow> downstream) {
        super(downstream, 5000,10000)
        this.chr = region.chr
        this.start = region.from
        this.end = region.to
        this.windowSize = windowSizes.max()*4
        currentPosition = -1
        
        this.windowSizes = windowSizes
        this.halfWindowSize = this.windowSize >> 1i
       
        log.info "Creating read count window with size $windowSize and layer sizes $windowSizes"
        window = new LayeredCoverageWindow(windowSize, windowSizes as int[])
    }
    
    @Override
    @CompileStatic
    void process(List<SAMRecord> records) {
        for(SAMRecord r : records) {
            process(r)
        }
    }
    
    int currentCount = 0
    
    int filteredCount
    
    int passFilter
    
    int totalReads
    
    @CompileStatic
    void process(SAMRecord r) {
        
        ++totalReads
        
        if(!filterFn.is(null)) {
             if(filterFn.isFilteredOut(r)) {
                 ++filteredCount
                 return
             }
        }
        
        ++passFilter

        final int pos = r.alignmentStart

        boolean verbose = false
        
        if(pos == 27168291) {
            println "Debug position"
        }
        
        if(currentPosition<0) {
            currentPosition = pos
        }

        // If the read is at the current position, increment that position
        if(pos == currentPosition) {
            ++currentCount
            return
        }
        
        // Pos must be greater than current position
        assert pos > currentPosition
        
        while(currentPosition<pos) {
            
            // Since we have left the previous position, send the current
            // counts downstream
            if(currentPosition>=start && currentPosition<end) {
               sendDownstream(new LayeredCounts(currentPosition-halfWindowSize, this.window.counts)) 
            } 
            
            this.addCurrentPosition()
            
            currentCount = 0
            
            ++currentPosition
        }
        
        // We have now advanced forward so that currentPosition == pos, and the 
        // read we are actually counting is at this position: here is where we count it
        ++currentCount
    }
    
    void addCurrentPosition() {
        // Shift the previous value into the window
        window.add(currentCount)
    }

    @Override
    public void onEnd() {
        
       // We have ended where the reads ended, not necessarily where the requested
       // window ended. Keep invoking callback until we do it for every position 
       // requested
       if(currentPosition < 0)
           currentPosition = start - 1
       while(currentPosition<end) {
           window.add(0)
           ++currentPosition
           sendDownstream(new LayeredCounts(currentPosition, window.counts))
       }
    }
}
