package px

import groovy.transform.CompileStatic
import groovy.util.logging.Log

@CompileStatic
@Log
class LayeredCounts {
    public LayeredCounts(int pos, LayerCount [] layers) {
        super();
        this.pos = pos;
        this.counts = layers
    }
    
    public LayerCount [] counts
    
    int pos
    
    @Override
    String toString() {
        counts.join(', ')
    }
}

