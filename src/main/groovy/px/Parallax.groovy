package px

import java.util.concurrent.atomic.AtomicInteger

import gngs.*
import px.CNVCaller
import px.Contrastor
import px.LayeredCounts
import px.ReadCountFilter
import px.ReadCounterActor
import graxxia.ThresholdRange
import graxxia.Thresholder
import groovy.transform.CompileStatic
import groovy.util.logging.Log
import groovyx.gpars.GParsPool
import htsjdk.samtools.SAMRecord

@Log
class Parallax extends ToolBase implements ReadCountFilter {
    
    static List<Short> DEFAULT_WINDOW_SIZES = [1000,1500,2000,2500,3000,3500,4000,4500,5000]
    
    List<Short> windowSizes = DEFAULT_WINDOW_SIZES.clone()
    
    SAM bam 

    static void main(String [] args) {
        cli('[options] <bam>', args) {
            'L' 'Region(s) to scan (unless -chr provided)', args: 1, required: false
            'e' 'File to write convolution error signal to', args:1, required:false
            'c' 'File to write layer component counts to', args:1, required:false
            'dell' 'File to write deletion layer likelihoods to', args:1, required:false
            'dupl' 'File to write duplication layer likelihoods to', args:1, required:false
            'n' 'How many threads to use (4)', args:1, required:false
            'd' 'Debug given region', args:1, required: false
            'minq' 'Minimum mapping quality for reads to include in counting (30)', args:1, required: false
            'w' 'Comma separated list of window sizes to use ('+DEFAULT_WINDOW_SIZES.join(",")+'), or single value as max', args:1, required: false
            'chr' 'The chromosome to process (unless -L provided)', args:1, required:false
            'o' 'Output file to write CNV calls to', args:1, required: true
        } 
    }
    
    int minimumMappingQuality = 30i
        
    Regions regions
    
    /**
     * The chromosome being processed
     */
    String chr
    
    @Override
    public void run() {
        
        String bamPath = opts.arguments()[0]
        if(bamPath == null) {
            System.err.println "Error: please provide a BAM file as input"
            cli.usage()
            System.exit(1)
        }
        
        this.bam = new SAM(bamPath)
        
        if(opts.w) {
            this.windowSizes = opts.w.tokenize(',')*.trim()*.toShort()
        }
        
        if(opts.minq) {
            this.minimumMappingQuality = opts.minq.toInteger()
        }
        
        if(!opts.L && !opts.chr) {
            System.err.println "\nError: please provide either -L or -chr to specify the region to analyse\n"
            System.exit(1)
        }
        
        if(opts.L) {
            if(new File(opts.L).exists()) {
                regions = new BED(opts.L).load()
            }
            else
                regions = new Regions([new Region(this.opts.L)])
        }
                
        if(opts.chr) {
            if(regions != null) {
                regions = regions.grep { it.chr == opts.chr } as Regions
            }
            else {
                Integer contigSize = new SAM(opts.arguments()[0]).contigs[opts.chr]
                regions = new Regions([new Region(opts.chr, 0, contigSize)])
            }
        }
        else {
            List<String> uniqueChrs = regions*.chr.unique()
            if(uniqueChrs.size()>1)
                throw new IllegalArgumentException("You supplied a BED file with multiple chromosomes:\n\n${uniqueChrs.join(',')}.\n\nThis program only processes one chromosome at a time. Please specify -chr or supply a BED file with only one chromosome")
        }
        
        regions = regions.widen(100000).reduce()
        
        this.chr = regions[0].chr
        
        log.info "Scanning ${Utils.humanBp(regions.size())} in ${chr} for regions of contrasting coverage depth with widths ${windowSizes.join(', ')}"
        
//        int pos = 0
//        ProgressCounter progress = new ProgressCounter(withRate:true, extra: {
//            "Last position: $pos"
//        })
//        
        int concurrency = opts.n ? opts.n.toInteger() : 4
        
        List windowSizesToProcess = windowSizes.size() == 1 ? (1000..windowSizes.max()).step(500) : windowSizes 
        
        Utils.withWriters([opts.e, opts.c, opts.dell, opts.dupl, opts.o]) { Writer e, Writer c, Writer dell, Writer dupl, Writer o ->
            CNVCaller duplicationCaller = new CNVCaller(chr, [1.5d, 1.5d], e, c, dupl, null)
            CNVCaller deletionCaller = new CNVCaller(chr, [0.5d, 0.5d], null, null, dell, duplicationCaller)
            duplicationCaller.start()
            deletionCaller.start()
            
            Contrastor contrastor = new Contrastor(windowSizesToProcess, deletionCaller)
            contrastor.start()
            
            for(Region region in regions) {
                        
                ReadCounterActor counter = new ReadCounterActor(region, windowSizesToProcess, contrastor)
                counter.filterFn = this
                counter.start()
                        
                bam.eachRecord(region.widen(10000,0), counter)
                counter.sendStop()
                counter.join()
                
                log.info "Filtered ${counter.filteredCount}/${counter.totalReads} reads (${Utils.perc(counter.filteredCount/(counter.totalReads+1))}) in scan of $region"
            }
                        
            [contrastor, deletionCaller, duplicationCaller].each { RegulatingActor actor ->
                actor.sendStop()
                actor.join()
            }
            
            log.info "Deletions:\t${deletionCaller.filteredTooSmall}/${deletionCaller.rawSignificantRegions} (${Utils.perc(deletionCaller.filteredTooSmall/Math.max(1,deletionCaller.rawSignificantRegions))}) CNV regions were filtered due to falling below size threshold"
            log.info "Duplications:\t${duplicationCaller.filteredTooSmall}/${duplicationCaller.rawSignificantRegions} (${Utils.perc(duplicationCaller.filteredTooSmall/Math.max(1,duplicationCaller.rawSignificantRegions))}) CNV regions were filtered due to falling below size threshold"
            
            this.writeCNVs(o, windowSizesToProcess, deletionCaller, duplicationCaller)
        }
        
//        progress.end()
    }
    
    void writeCNVs(Writer w, List windowSizes, CNVCaller delCaller, CNVCaller dupCaller) {
        String chr = this.regions[0].chr
        Regions widthedCnvs = windowSizes.collect { int windowSize ->
            List dels = delCaller.cnvs[windowSize].collect { GRange cnv ->
                new Region(chr, cnv, cnv: cnv.extra, width: windowSize, type: 'DEL')
            } 
            
            List dups = dupCaller.cnvs[windowSize].collect { GRange cnv ->
                new Region(chr, cnv, cnv: cnv.extra, width: windowSize, type:'DUP')
            }             
            
            return dels + dups
        }.flatten() as Regions
        
        
//        widthedCnvs.save('raw.cnvs.bed', extra: { it.type + '-' + it.width + '-' + it.cnv.stats.max })
        
//        widthedCnvs.save('test.tmp.cnv.bed', extra: { 
//            if(it.cnv.stats.max.isNaN()) {
//                println "oh no, nan"
//            }
//                
//            it.cnv.stats.max 
//        })
        
        log.info "Found ${widthedCnvs.numberOfRanges} CNVs before flattening redundant width calls"
        
//        RangeIndex.xxDebug = true
//        RegionIterator.xxDebug = true
        
        Regions compactedCnvs = widthedCnvs.reduce { GRange r1, GRange r2 ->
            def result = r1.extra.cnv.stats.max > r2.extra.cnv.stats.max ? r1.extra : r2.extra
            
            assert result != null
            
            assert result.cnv != null
            
            return result
        }
        
        log.info "Found ${compactedCnvs.numberOfRanges} CNVs after flattening redundant width calls"
        
        compactedCnvs.each {  r ->
            def cnv = (r.extra instanceof Region) ? r.extra.cnv : r.extra
            w.println([
                r.chr,
                r.from,
                r.to,
                r.type,
                cnv.stats.max
            ].join('\t'))
        }
    }
    
    void writeCNVsOld(Writer w, List windowSizes, CNVCaller printer) {
        String chr = this.regions[0].chr
        Regions cnvRegions = [windowSizes, printer.thresholders].transpose().collect { int windowSize, Thresholder t ->
            t.ranges.collect { ThresholdRange r ->
                new Region(chr, r.from, r.to, cnv: r)
            }
        }.flatten() as Regions
        
        log.info "Found ${cnvRegions.numberOfRanges} raw CNV regions"
        
        Regions compacted = cnvRegions.widen(100).reduce{ r1, r2 -> 
            r1.extra.cnv.stats.max > r2.extra.cnv.stats.max ? r1.extra : r2.extra
        }.widen(-100)
        
        log.info "Compaction reduced regions to ${compacted.numberOfRanges} raw CNV regions"
        
        compacted.save(w, sorted:true, extra: { it.extra.cnv.stats.max })
    }
    
    @Override
    @CompileStatic
    public boolean isFilteredOut(SAMRecord r) {
        return r.mappingQuality < minimumMappingQuality;
    }
}
