package px;

import java.util.Arrays;

import groovy.transform.CompileStatic;
import groovy.util.logging.Log;

/**
 * Captures a snapshot of the current count statistics from a given
 * layer within a layered coverage window.
 * <p>
 * Note: a layer consists of multiple components which each have a separate count.
 * 
 * @author Simon Sadedin
 */
@CompileStatic
class LayerCount {
    
    public LayerCount(LayeredCoverageWindow.LayerComponent... components) {
        
        final int numComponents = components.length;
        
        assert numComponents == 3;
        
        this.counts = new int[numComponents];
        this.widths = new int[numComponents];
        this.readsPerBase = new double[numComponents];
        
        for(int i=0; i<numComponents; ++i) {
            this.counts[i] = components[i].count;
            this.widths[i] = components[i].width;
            this.readsPerBase[i] = ((double)(this.counts[i])) / ((double)this.widths[i]);
        }
    }
    
    public int [] counts;
    public int [] widths;
    public double [] readsPerBase;
    
    public String toString() {
        
        StringBuilder b = new StringBuilder();
        for(int i=0; i<counts.length; ++i) {
            if(i>0)
                b.append(", ");
            b.append(String.valueOf(counts[i]));
        }
        
        return "Layer(width:" + widths[1] + ",reads:" + b.toString() + ")";
    }
}

