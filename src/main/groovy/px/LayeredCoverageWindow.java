package px;

import java.util.List;
import java.util.logging.Logger;

public class LayeredCoverageWindow {
    
    static Logger log = Logger.getLogger("LayeredCoverageWindow");
    
    public class LayerComponent {
        final public int startOffset;
        final public int endOffset;
        public int count = 0;
        final public int width;
        public LayerComponent(int startOffset, int endOffset) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.width = endOffset - startOffset;
        }
        
        void shift() {
            // Drop the value at the leading edge
            int old = this.count;
            this.count = this.count - getAt(startOffset) + getAt(endOffset);
//            System.out.println("Layer " + startOffset + "-" + endOffset + " total: " + old + " to " + count);
        }
    }
    
    int offset = 0;

    short [] values;

    public LayerComponent [] components;
    
    final int numComponents;

    private final int numLayers;
    
    final int windowSize;
    
    private final int maxPosition;
    
    LayeredCoverageWindow(int windowSize, int [] layerSizes) {
        this.windowSize = windowSize;
        this.maxPosition = windowSize-1;
        this.values = new short[windowSize];
        this.numLayers = layerSizes.length; 
        final int midPoint = Math.round(windowSize/2);
        this.numComponents = numLayers*3;
        this.components = new LayerComponent[numComponents]; // at the moment, 3 components per layer
        
        int totalLayers = 0;
        for(int i = 0; i < layerSizes.length; i++) {
            int layerSize = layerSizes[i];
            log.info("Creating layer of size " + layerSize);
            final int halfLayerSize = layerSize/2;
            
            // The layer structure has 3 components designed to let us measure 3 changes in level
            // associated with a CNV:
            //
            //             left                        midpoint                     right
            //     |----------------------|---------------o----------------|---------------------|
            //      <--- layerSize/2 --->  <--------- layerSize ----------> <--- layerSize/2 --->

//            this.components[totalLayers++] = new LayerComponent(midPoint-layerSize,midPoint-halfLayerSize);
//            this.components[totalLayers++] = new LayerComponent(midPoint - halfLayerSize,midPoint + halfLayerSize);
//            this.components[totalLayers++] = new LayerComponent(midPoint+halfLayerSize, midPoint + layerSize);
            
            
            // The layer structure has 3 components designed to let us measure 3 changes in level
            // associated with a CNV:
            //
            //             left                        midpoint                     right
            //     |----------------------|---------------o----------------|---------------------|
            //      <--- layerSize --->    <--------- layerSize ---------->  <--- layerSize --->

            this.components[totalLayers++] = new LayerComponent(midPoint - layerSize - halfLayerSize -1, midPoint-halfLayerSize-1);
            this.components[totalLayers++] = new LayerComponent(midPoint - halfLayerSize, midPoint + halfLayerSize+1);
            this.components[totalLayers++] = new LayerComponent(midPoint+halfLayerSize+1, midPoint + layerSize + halfLayerSize+1); 
        }
        
        // Check the components stay within the bounds
        for(LayerComponent c : components) {
            if(c.endOffset >= windowSize)
                throw new IllegalArgumentException("Layer component (size = " + (c.endOffset-c.startOffset) + ") extends beyonds the bounds of the window size (start = " + c.startOffset + ", end = " +c.endOffset);
           if(c.startOffset < 0)
                throw new IllegalArgumentException("Layer component (size = " + (c.endOffset-c.startOffset) + ") extends beyonds the bounds of the window size (start = " + c.startOffset + ", end = " +c.endOffset);
        }
    }
    
    public LayeredCoverageWindow leftShift(int value) {
        this.add(value);
        return this;
    }
    
    public LayeredCoverageWindow leftShift(List<Integer> values) {
        for(Integer value : values) {
            this.add(value);
        }
        return this;
    } 
    
    /**
     * Adds a value to the window, shifting it to the right, and updating all the totals.
     * 
     * @param value
     */
    public void add(int value) {
        for(int i=0; i<numComponents; ++i) {
            components[i].shift();
        }
        ++offset;
        setAt(maxPosition, value);
    }
    
    public void setAt(int position, int value) {
        if(position<0 || position>maxPosition)
            throw new IndexOutOfBoundsException(String.format("Index %d is outside the bounds of window of size %d", position, windowSize));
        values[(offset + position) % windowSize] = (short) value;
    }
    
    public short getAt(int position) {
        if(position<0 || position>maxPosition)
            throw new IndexOutOfBoundsException(String.format("Index %d is outside the bounds of window of size %d", position, windowSize));
        return values[(offset + position) % windowSize];
    }
    
    /**
     * Captures a snapshot of the current counts from each configured layer for this window
     */
    public LayerCount [] getCounts() {
        LayerCount [] counts = new LayerCount[numLayers];
        int componentIndex = 0;
        for(int i=0; i<numLayers; ++i) {
            LayerComponent c1 = this.components[componentIndex++];
            LayerComponent c2 = this.components[componentIndex++];
            LayerComponent c3 = this.components[componentIndex++];
            counts[i] = new LayerCount(c1, c2, c3);
        }
        return counts; 
    }
    
    String dump() {
        StringBuilder result = new StringBuilder();
        
        for(int i=0; i<windowSize; ++i) {
//            if(i>0)
//                result.append("  ");
            result.append(String.format("%4d",i+1));
        } 
        result.append("\n   ");
        for(int i=0; i<windowSize; ++i) {
            if(i>0)
                result.append(",  ");
            result.append(getAt(i));
        }
        return result.toString();
    }
}
