# Parallax

Parallax is a whole genome CNV caller for detecting CNVs in the range 500bp to 20kbp

To build parallax:

 - Ensure you have Java 8 or newer


```bash
git clone https://gitlab.com/ssadedin/parallax.git
cd parallax
./gradlew jar
```

Now you can execute:

```
java -jar build/libs/parallax.jar -chr chr22 -l cnvs.lh.tsv  -o cnvs.tsv  /some/bam/file
```

Candidate CNVs will be written in tab separated format to cnvs.lh.tsv.
